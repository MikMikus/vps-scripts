# VPS configure scripts #

Several scripts that automate the work

## Ubuntu version - install script

Includes:

* update & upgrade
* ready to use, the latest version of the docker & docker-compose
* git

### AWS

```sh
wget -O - https://bitbucket.org/MikMikus/vps-scripts/raw/master/ubuntu/first-install-aws | bash
```

### OVH

*tested on ubuntu 20.04*

Additionally includes:

* firewall nftables with opened ports: 80, 443, 22
* fail2ban for sshd
* deny ssh for root
* new user with ssh access

During installation you will be ask about new user name and password.

```sh
source <(curl -s https://bitbucket.org/MikMikus/vps-scripts/raw/master/ubuntu/first-install)
```

## Services

### Proxy server

Install proxy server with auto generate certificates for https.

```
curl -s https://bitbucket.org/MikMikus/vps-scripts/raw/master/services/proxy-install | sudo bash -s -- your@email.com
```

The installation creates a systemd service so it is manageable via systemctl:

```
sudo systemctl status proxy.service
```

Now you can use "VIRTUAL_HOST" "LETSENCRYPT_HOST" variables and "proxy" network in your docker to automatically proxy your apps with https.

Example:

```
version: "3.3"

services:
  test:
    image: "httpd:alpine"
    environment:
      VIRTUAL_HOST: example.domain
      LETSENCRYPT_HOST: example.domain
    networks:
      - proxy

networks:
  proxy:
    external:
      name: proxy
```

More info about proxy server:

[Used repo](https://github.com/evertramos/nginx-proxy-automation),
[acme](https://github.com/nginx-proxy/acme-companion),
[nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)


#### Basic Authentication

Add basic auth or add next user:

```
sudo docker run --rm -ti xmartlabs/htpasswd <username> <password> | sudo tee -a /srv/proxy/data/htpasswd/<domain>
```

Remove basic auth

```
sudo rm /srv/proxy/data/htpasswd/<domain>
sudo docker restart proxy-web-auto
```

Remove specyfic user:

```
sudo sed -i '/^<username>/d' /srv/proxy/data/htpasswd/<domain>
```
